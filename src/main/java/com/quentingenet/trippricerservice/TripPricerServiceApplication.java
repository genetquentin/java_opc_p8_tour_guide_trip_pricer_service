package com.quentingenet.trippricerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import tripPricer.TripPricer;

@SpringBootApplication
public class TripPricerServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(TripPricerServiceApplication.class, args);
  }

  @Bean
  public TripPricer getTripPricer() {
    return new TripPricer();
  }

}
