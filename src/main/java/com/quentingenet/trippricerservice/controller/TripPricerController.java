package com.quentingenet.trippricerservice.controller;

import com.quentingenet.trippricerservice.service.ProviderProxy;
import com.quentingenet.trippricerservice.service.TripPricerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class TripPricerController {

  private final Logger logger = LoggerFactory.getLogger(TripPricerController.class);

  @Autowired
  private TripPricerService tripPricerService;

  @GetMapping("/prices")
  public List<ProviderProxy> getPrice(@RequestParam("tripPricerApiKey") String tripPricerApiKey,
                                      @RequestParam("userId") UUID userId,
                                      @RequestParam("numberOfAdults") int numberOfAdults,
                                      @RequestParam("numberOfChildren") int numberOfChildren,
                                      @RequestParam("tripDuration") int tripDuration,
                                      @RequestParam("cumulatativeRewardPoints") int cumulatativeRewardPoints) {
    logger.info("GET /prices?tripPricerApiKey={}?userId={}?numberOfAdults={}?numberOfChildren={}?tripDuration={}?cumulatativeRewardPoints={}", tripPricerApiKey, userId, numberOfAdults, numberOfChildren, tripDuration, cumulatativeRewardPoints);
    return tripPricerService.getPrice(tripPricerApiKey, userId, numberOfAdults, numberOfChildren, tripDuration, cumulatativeRewardPoints);
  }
}
