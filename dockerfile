FROM openjdk:8-alpine
COPY build/libs/trippricerservice-0.0.1-SNAPSHOT.jar trippricer-service.jar
CMD ["java", "-jar", "trippricer-service.jar"]
